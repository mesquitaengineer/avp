﻿

namespace AVP.domain.Interfaces.UnitOfWorks.Common
{
   public interface IUnitOfWorkFactory
    {
        IUnitOfWork Create(string contextType);
    }
}
