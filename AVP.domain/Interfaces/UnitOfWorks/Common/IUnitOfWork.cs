﻿using AVP.domain.Interfaces.Repositories;
using AVP.domain.Interfaces.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace AVP.domain.Interfaces.UnitOfWorks.Common
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> CreateGenericRepository<TEntity>()
            where TEntity : class;

        IServicoRepository CreateServicoRepository();

        ISetorRepository CreateSetorRepository();

        void Commit();
        
    }
}
