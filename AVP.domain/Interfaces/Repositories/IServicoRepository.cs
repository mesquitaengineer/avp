﻿using AVP.domain.Entities;
using AVP.domain.Interfaces.Repositories.Common;

namespace AVP.domain.Interfaces.Repositories
{
    public interface IServicoRepository : IRepository<Servico>
    {
        Servico GetByCodigoAtive(int codigo);
    }
}
