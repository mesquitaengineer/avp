﻿using System.Collections.Generic;

namespace AVP.domain.Interfaces.Repositories.Common
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Add(TEntity obj);
        TEntity GetByCodigo(int? codigo);
        IEnumerable<TEntity> GetAll();
        void Update(TEntity obj);
        void Remove(TEntity obj);
    }
}
