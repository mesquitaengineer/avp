﻿using AVP.domain.Entities;
using AVP.domain.Interfaces.Repositories.Common;

namespace AVP.domain.Interfaces.Repositories
{
    public interface ISetorRepository : IRepository<Setor>
    {
        Setor GetByCodigoAtive(int codigo);

    }
}
