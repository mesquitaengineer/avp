﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AVP.domain.Entities
{
    public class Servico
    {
        [Key]
        public int codigo { get; set; }
        public string descricao { get; set; }
        public bool ativo { get; set; } = true;


    }
}
