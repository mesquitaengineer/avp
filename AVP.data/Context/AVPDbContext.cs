﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using AVP.data.Context.Common;
using AVP.data.Mappings.Meta;
using AVP.domain.Entities;
using System.IO;
using System.Linq;

namespace AVP.data.Context
{
    public class AVPDbContext : BaseDbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseSqlServer(config.GetConnectionString("AVP"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
          
            modelBuilder.ApplyConfiguration(new ServicoMap());
            modelBuilder.ApplyConfiguration(new SetorMap());

        }

        private DbSet<Servico> _servicos { get; set; }
        private DbSet<Setor> _setores { get; set; }


        public override IQueryable<Servico> servicos { get { return _servicos; } set { } }
        public override IQueryable<Setor> setores { get { return _setores; } set { } }

    }
}