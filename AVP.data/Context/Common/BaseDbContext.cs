﻿using Microsoft.EntityFrameworkCore;
using AVP.domain.Entities;
using System.Linq;

namespace AVP.data.Context.Common
{


   
    public abstract class BaseDbContext : DbContext
    {
        public abstract IQueryable<Servico> servicos { get; set; }
        public abstract IQueryable<Setor> setores { get; set; }
    }
}

