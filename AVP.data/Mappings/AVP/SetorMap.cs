﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using AVP.domain.Entities;
using AVP.util.Data;

namespace AVP.data.Mappings.Meta
{
    public class SetorMap : IEntityTypeConfiguration<Setor>
    {
        public void Configure(EntityTypeBuilder<Setor> builder)
        {
            ControleConversao CC = new ControleConversao();

            builder.ToTable("tblSetores");

            builder.Property(b => b.codigo).HasColumnName("setor_id");
            builder.Property(b => b.descricao).HasColumnName("setor_nome");
            builder.Property(b => b.ativo).HasColumnName("setor_ativa");


            //var converter = new CastingConverter<byte, string>();



        }
    }
}
