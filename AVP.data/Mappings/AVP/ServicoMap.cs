﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using AVP.domain.Entities;
using AVP.util.Data;

namespace AVP.data.Mappings.Meta
{
    public class ServicoMap : IEntityTypeConfiguration<Servico>
    {
        public void Configure(EntityTypeBuilder<Servico> builder)
        {
            ControleConversao CC = new ControleConversao();

            builder.ToTable("tblServicos");

            builder.Property(b => b.codigo).HasColumnName("servico_id");
            builder.Property(b => b.descricao).HasColumnName("servico_nome");
            builder.Property(b => b.ativo).HasColumnName("servico_ativo");


            //var converter = new CastingConverter<byte, string>();



        }
    }
}
