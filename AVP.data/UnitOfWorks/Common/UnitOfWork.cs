﻿using AVP.data.Context.Common;
using AVP.data.Repositories;
using AVP.data.Repositories.Common;
using AVP.domain.Interfaces.Repositories;
using AVP.domain.Interfaces.Repositories.Common;
using AVP.domain.Interfaces.UnitOfWorks.Common;



namespace AVP.data.UnitOfWorks
{
    public class UnitOfWork<TContext> : IUnitOfWork
    where TContext : BaseDbContext, new()
    {
        private TContext _dbContext;

        public UnitOfWork()
        {
            _dbContext = new TContext();
        }

        public IRepository<TEntity> CreateGenericRepository<TEntity>()
            where TEntity : class
        {
            return new Repository<TEntity>(_dbContext);
        }

        public IServicoRepository CreateServicoRepository()
        {
            return new ServicoRepository(_dbContext);
        }

        public ISetorRepository CreateSetorRepository()
        {
            return new SetorRepository(_dbContext);
        }


        public void Commit()
        {
            _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
        
    }

}
