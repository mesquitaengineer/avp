﻿using Microsoft.Extensions.Configuration;
using AVP.data.Context;
using AVP.domain.Interfaces.UnitOfWorks.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AVP.data.UnitOfWorks.Common
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        public IUnitOfWork Create(string controllertType)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            string contextType = config.GetSection("SettingConnectionStringsByController").GetValue<string>(controllertType);

            switch (contextType)
            {
                case "AVP":
                    return new UnitOfWork<AVPDbContext>();
                //case "AnotherModelContainer":
                    //    return new UnitOfWork<AnotherModelContainer>();
            }

            throw new ArgumentException("Unknown contextType...");
        }
    }
}
