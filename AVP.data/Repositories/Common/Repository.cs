﻿using Microsoft.EntityFrameworkCore;
using AVP.data.Context;
using AVP.data.Context.Common;
using AVP.domain.Interfaces.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AVP.data.Repositories.Common
{
    public class Repository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : class  
    {
        protected readonly BaseDbContext db;
        //private DbContext _dbContext;

        public Repository()
        {
            db = new AVPDbContext();
        }

        public Repository(BaseDbContext dbContext)
        {
            db = dbContext;
        }

        public virtual void Add(TEntity obj)
        {
            db.Add(obj);
            db.SaveChanges();
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return db.Set<TEntity>().ToList();
        }

        public virtual TEntity GetByCodigo(int? codigo)
        {
            return db.Set<TEntity>().Find(codigo);
        }

        public virtual void Remove(TEntity obj)
        {
            db.Set<TEntity>().Remove(obj);
            db.SaveChanges();
        }

        public virtual void Update(TEntity obj)
        {
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}