﻿using AVP.data.Context.Common;
using AVP.data.Repositories.Common;
using AVP.data.UnitOfWorks.Common;
using AVP.domain.Entities;
using AVP.domain.Interfaces.Repositories;
using AVP.domain.Interfaces.UnitOfWorks.Common;
using System.Collections.Generic;
using System.Linq;

namespace AVP.data.Repositories
{
    public class SetorRepository : Repository<Setor>, ISetorRepository
    {

        public SetorRepository() : base()
        {

        }

        public SetorRepository(BaseDbContext dbContext) : base(dbContext)
        {

        }

        public override IEnumerable<Setor> GetAll() =>
             db.setores.Where(p => p.ativo).ToList();

        public Setor GetByCodigoAtive(int setor) =>
            db.setores.Where(p => p.ativo && p.codigo == setor).FirstOrDefault();
    }
}
