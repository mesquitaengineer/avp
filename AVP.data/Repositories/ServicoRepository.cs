﻿using AVP.data.Context.Common;
using AVP.data.Repositories.Common;
using AVP.data.UnitOfWorks.Common;
using AVP.domain.Entities;
using AVP.domain.Interfaces.Repositories;
using AVP.domain.Interfaces.UnitOfWorks.Common;
using System.Collections.Generic;
using System.Linq;

namespace AVP.data.Repositories
{
    public class ServicoRepository : Repository<Servico>, IServicoRepository
    {

        public ServicoRepository() : base()
        {
            
        }

        public ServicoRepository(BaseDbContext dbContext) : base(dbContext)
        {

        }

        public override IEnumerable<Servico> GetAll() =>
             db.servicos.Where(p => p.ativo).ToList();

        public Servico GetByCodigoAtive(int codigo) =>
            db.servicos.Where(p => p.ativo && p.codigo == codigo).FirstOrDefault();

    }
}
