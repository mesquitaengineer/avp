﻿using Microsoft.Extensions.DependencyInjection;
using AVP.data.Repositories;
using AVP.data.UnitOfWorks.Common;
using AVP.domain.Interfaces.Repositories;
using AVP.domain.Interfaces.UnitOfWorks.Common;

namespace AVP.ioc
{
    public static class NativeInjectorConfig
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            //services.AddScoped<IPessoaRepository, PessoaRepository>();
           // services.AddScoped<IFuncaoRepository, FuncaoRepository>();
            //services.AddScoped<ICargoRepository, CargoRepository>();
            services.AddScoped<IUnitOfWorkFactory, UnitOfWorkFactory>(); 
           // services.AddScoped<ITipoLocalRepository, TipoLocalRepository>();
            //services.AddScoped<ILocalRepository, LocalRepository>();
            //services.AddScoped<IDesignacaoRepository, DesignacaoRepository>();
            //services.AddScoped<IMembroRepository, MembroRepository>();
            //services.AddScoped<IComarcaRepository, ComarcaRepository>();
            //services.AddScoped<IFinalidadeRepository, FinalidadeRepository>();
        }
    }
}
