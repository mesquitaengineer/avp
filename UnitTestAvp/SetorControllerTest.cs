using AVP.Controllers;
using AVP.data.UnitOfWorks.Common;
using AVP.domain.Interfaces.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using AVP.domain.Entities;
using AVP.domain.Interfaces.UnitOfWorks.Common;

namespace UnitTestAvp
{
    [TestClass]
    public class SetorControllerTest
    {

        SetorController _controller;
        ISetorRepository _service;
        IUnitOfWorkFactory _factory;

        public SetorControllerTest()
        {
            _service = new SetorRepositoryFake();
            _controller = new SetorController(_factory);
        }

        [Fact]
        public void Get_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.Get();

            // Assert
            Assert.IsInstanceOfType(okResult, typeof(OkObjectResult));
        }

        [Fact]
        public void Get_WhenCalled_ReturnsNoContent()
        {
            // Act
            var okResult = _controller.Get();

            // Assert
            Assert.IsInstanceOfType(okResult, typeof(ObjectResult));
        }


        [Fact]
        public void Get_WhenCalled_ReturnsAllItems()
        {
            // Act
            var okResult = _controller.Get() as OkObjectResult;

            // Assert
            Assert.IsInstanceOfType(okResult.Value, typeof(List<Setor>));
            Assert.Equals(3, ((List<Setor>)okResult.Value).Count);
        }

















        [Fact]
        public void Add_ValidObjectPassed_ReturnsCreatedResponse()
        {
            // Arrange
            Setor testItem = new Setor()
            {
                descricao = "Teste",
                ativo = true
            };

            // Act
            var createdResponse = _controller.Add(testItem);

            // Assert
            //Assert.IsType<CreatedAtActionResult>(createdResponse);
        }


       
    }
}
