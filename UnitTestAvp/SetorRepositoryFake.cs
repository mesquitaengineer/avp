﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AVP.data.Context.Common;
using AVP.data.Repositories.Common;
using AVP.domain.Entities;
using AVP.domain.Interfaces.Repositories;
using AVP.domain.Interfaces.Repositories.Common;

namespace UnitTestAvp
{
    public class SetorRepositoryFake :  Repository<Setor>, ISetorRepository
    {
       
        private readonly List<Setor> _setores;

        public SetorRepositoryFake() : base()
        {
            _setores = new List<Setor>()
            {
                new Setor() { codigo = 1,
                    descricao = "Financeiro", ativo=true },
                new Setor() { codigo = 2,
                    descricao = "Financeiro", ativo=true },
                new Setor() { codigo = 3,
                    descricao = "Financeiro", ativo=true },
            };
        }

        public SetorRepositoryFake(BaseDbContext dbContext) : base(dbContext)
        {

        }

        public override IEnumerable<Setor> GetAll()
        {
            return _setores;
        }

        public Setor AddSetor(Setor setor)
        {
            setor.codigo = _setores.Last().codigo+1;
            _setores.Add(setor);
            return setor;
        }

        public Setor GetById(int codigo)
        {
            return _setores.Where(a => a.codigo == codigo)
                .FirstOrDefault();
        }

        public Setor GetByCodigoAtive(int codigo)
        {
            return _setores.Where(a => a.codigo == codigo && a.ativo)
                .FirstOrDefault();
        }

        public void Remove(int codigo)
        {
            var setor = _setores.First(a => a.codigo == codigo);
            _setores.Remove(setor);
        }

       
    }
}
