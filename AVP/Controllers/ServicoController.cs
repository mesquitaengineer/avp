﻿using Microsoft.AspNetCore.Mvc;
using AVP.domain.Entities;
using AVP.domain.Interfaces.Repositories;
using AVP.domain.Interfaces.UnitOfWorks.Common;
using AVP.util.Handle.Error;
using System.Collections.Generic;
using System.Linq;

namespace AVP.Controllers
{
    [Produces("application/json")]
    [Route("api/Servico")]
    public class ServicoController : Controller
    {
        private readonly IUnitOfWorkFactory _factory;

        private IUnitOfWork _unitOfWork;

        private  IServicoRepository _servicoRepository;

        public ServicoController(IUnitOfWorkFactory factory)
        {
            _factory = factory;
            _unitOfWork = _factory.Create(this.GetType().Name);
            _servicoRepository = _unitOfWork.CreateServicoRepository();
        }
        
        [HttpGet(Name = "GetServico")]
        public IActionResult Get()
        {
            IEnumerable < Servico > lServico = _servicoRepository.GetAll();
            if (lServico.Count() > 0)
                return Ok(lServico);
            else
                return new ObjectResult(new NoContentError());
        }

        [HttpGet("{servico}", Name = "GetServicoByServico")]
        public IActionResult GetByCodigoAtive(string codigo)
        {
            int value;
            if (string.IsNullOrWhiteSpace(codigo) || !int.TryParse(codigo, out value))
                return BadRequest();

            Servico servico = _servicoRepository.GetByCodigoAtive(value);
            if (servico != null)
                return Ok(servico);
            else
                return new ObjectResult(new NoContentError());
            
        }
    }
}