﻿using Microsoft.AspNetCore.Mvc;
using AVP.domain.Entities;
using AVP.domain.Interfaces.Repositories;
using AVP.domain.Interfaces.UnitOfWorks.Common;
using AVP.util.Handle.Error;
using System.Collections.Generic;
using System.Linq;

namespace AVP.Controllers
{
    [Produces("application/json")]
    [Route("api/Setor")]
    public class SetorController : Controller
    {
        private readonly IUnitOfWorkFactory _factory;

        private IUnitOfWork _unitOfWork;

        private  ISetorRepository _setorRepository;

        public SetorController(IUnitOfWorkFactory factory)
        {
            _factory = factory;
            _unitOfWork = _factory.Create(this.GetType().Name);
            _setorRepository = _unitOfWork.CreateSetorRepository();
        }
        
        [HttpGet(Name = "GetSetor")]
        public IActionResult Get()
        {
            IEnumerable < Setor > lSetor = _setorRepository.GetAll();
            if (lSetor.Count() > 0)
                return Ok(lSetor);
            else
                return new ObjectResult(new NoContentError());
        }

        [HttpGet("{setor}", Name = "GetSetorBySetor")]
        public IActionResult GetSetorBySetor(string codigo)
        {
            int value;
            if (string.IsNullOrWhiteSpace(codigo) || !int.TryParse(codigo, out value))
                return BadRequest();

            Setor setor = _setorRepository.GetByCodigoAtive(value);
            if (setor != null)
                return Ok(setor);
            else
                return new ObjectResult(new NoContentError());
            
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult Add([Bind] Setor setor)
        {
            if (ModelState.IsValid)
            {
                _setorRepository.Add(setor);
                if (setor != null)
                    return Ok(setor);
                else
                    return new ObjectResult(new NoContentError());
            }
            else
            {
                return BadRequest();
            }
        }
    }
}