﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.DirectoryServices.AccountManagement;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Text;

namespace AVP.Controllers
{
    [Produces("application/json")]
    [Route("api/Auth")]
    public class AuthController : Controller
    {
        //private readonly IInstituicaoRepository _instituicaoRepository;


        [HttpGet("{user}/{password}")]
        public string GetAuthenticate(string user, string password)
        {
            string token;

            if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(password))
                return null;

            if (!ValiteUserAd(user, password))
            {
                return null;
            }

            token = GetBuildToken(user);

            // check if token exists
            if (token == null)
                return null;

            // authentication successful
            return token;
        }


        public bool ValiteUserAd(string username, string password)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            string LDAP_DOMAIN = config.GetSection(key: "Config")["LDAP_DOMAIN"] + ":" + config.GetSection(key: "Config")["LDAP_PORT"];

            using (var context = new PrincipalContext(ContextType.Domain, LDAP_DOMAIN, "service_acct_user", "service_acct_pswd"))
            {
                if (context.ValidateCredentials(username, password))
                {
                    return true;
                }
            }

            return false;
        }

        public string GetBuildToken(string user)
        {

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var claims = new[]
                {
                  new Claim(JwtRegisteredClaimNames.Sub, user),
                };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.GetSection(key: "Config")["SecretKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(config.GetSection(key: "Config")["Issuer"], config.GetSection(key: "Config")["Issuer"],
              expires: DateTime.Now.AddMinutes(Convert.ToInt16(config.GetSection(key: "Config")["JwtExpireMinutes"])),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}