﻿using AVP.util.Handle.Common.Error;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace AVP.Controllers
{
    [Produces("application/json")]
    [Route("/errors")]
    public class ErrorsController : ControllerBase
    {
        [Route("{code}")]
        public IActionResult Error(int code)
        {
            HttpStatusCode parsedCode = (HttpStatusCode)code;
            ApiError error = new ApiError(parsedCode, parsedCode.ToString());

            return new ObjectResult(error);
        }
    }
}