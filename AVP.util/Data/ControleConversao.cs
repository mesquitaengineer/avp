﻿using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AVP.util.Data
{
    public class ControleConversao
    {
        public ValueConverter converterBooleanoParaStringSouN = new BoolToStringConverter("N", "S");

        public ValueConverter converterBooleanoParaStringSouF = new BoolToStringConverter("F", "S");

        public ValueConverter converterBooleanoParaString1ou2 = new BoolToStringConverter("2", "1");


    }
}
