﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using AVP.util.Handle.Common.Error;

namespace AVP.util.Handle.Error
{
    public class InternalServerError : ApiError
    {
        public InternalServerError()
            : base(HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString())
        {
        }


        public InternalServerError(string message)
            : base(HttpStatusCode.InternalServerError, HttpStatusCode.InternalServerError.ToString(), message)
        {
        }
    }
}
