﻿using AVP.util.Handle.Common.Error;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace AVP.util.Handle.Error
{
    public class NoContentError : ApiError
    {
        public NoContentError()
            : base(HttpStatusCode.NoContent, HttpStatusCode.NoContent.ToString())
        {
        }


        public NoContentError(string message)
            : base(HttpStatusCode.NoContent, HttpStatusCode.NoContent.ToString(), message)
        {
        }
    }
}
