﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using AVP.util.Handle.Common.Error;

namespace AVP.util.Handle.Error
{
    public class NotFoundError : ApiError
    {
        public NotFoundError()
            : base(HttpStatusCode.NotFound, HttpStatusCode.NotFound.ToString())
        {
        }


        public NotFoundError(string message)
            : base(HttpStatusCode.NotFound, HttpStatusCode.NotFound.ToString(), message)
        {
        }
    }
}
